﻿
using System;
using System.Diagnostics;
using System.IO;

public class Cmd
{
    public static string ExecuteBashCommand(string command)
    {
        command = command.Replace("\"", "\"\"");

        var proc = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "/bin/bash",
                Arguments = "-c \"" + command + "\"",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            }
        };

        proc.Start();
        proc.WaitForExit();

        return proc.StandardOutput.ReadToEnd();
    }

    public static void RunWindowsCommand(string command)
    {
        string output = string.Empty;
        string error = string.Empty;

        ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", command);
        processStartInfo.RedirectStandardOutput = true;
        processStartInfo.RedirectStandardError = true;
        processStartInfo.UseShellExecute = false;

        Process process = Process.Start(processStartInfo);
        using (StreamReader streamReader = process.StandardOutput)
        {
            output = streamReader.ReadToEnd();
        }

        using (StreamReader streamReader = process.StandardError)
        {
            error = streamReader.ReadToEnd();
        }

        Console.WriteLine("The following output was detected:");
        Console.WriteLine(output);

        if (!string.IsNullOrEmpty(error))
        {
            Console.WriteLine("The following error was detected:");
            Console.WriteLine(error);
        }

        Console.Read();
    }
}