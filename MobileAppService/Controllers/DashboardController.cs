﻿

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

[Route("api/[controller]")]
public class DashboardController : Controller
{
    [HttpGet("run")]
    public string RunCommand(string command)
    {
        string s = Cmd.ExecuteBashCommand(command);
        return s;
    }

    [HttpGet("{Id}")]
    public DashboardServerEntry GetItem(string id)
    {
        Process[] processes = Process.GetProcesses();
        return null;
    }
}

public class DashboardServerEntry
{
    public string name { get; set; }
    public ProcessStartInfo startInfo { get; set; }

    public DashboardServerEntry(Process process)
    {
        name = process.ProcessName;
    }
}